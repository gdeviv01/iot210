#!/usr/bin/python
# =============================================================================
#        File : hue_mqtt_publish.py
# Description : publish data to an MQTT Broker for the Philips Hue
#      Author : Drew Gislsason
#        Date : 4/7/2017
# =============================================================================
# pip install paho-mqtt
import paho.mqtt.client as mqtt

# command will go over MQTT in form of:  HueCmd(Name): {"on":true}
PREFIX      = "HueCmd("
AFTERNAME   = "): "
topic_name = "iot210hue"

client = mqtt.Client()
client.connect("iot.eclipse.org", 1883, 60)
client.loop_start()

print "\nhue_mqtt_publish.py\n"
print "example: {\"on\":true, \"hue\":0, \"sat\":0, \"bri\":254}\n"

name = raw_input("Enter your name: ")

while True:
  s = raw_input("Enter cmd to publish: ")
  client.publish(topic_name, PREFIX + name + AFTERNAME + s)
