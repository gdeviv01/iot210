#!/bin/bash

# Created referencing https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs

# The generate the name of the keys from the your local hostname 
# This may be the domain name of the site you would like to protect with encryption
domain=$(hostname)

# Bit level encryption (standard is 2048)
rsa_bits=2048

# This is the command that generates the RSA key pair
openssl req \
  -newkey rsa:$rsa_bits \
  -nodes \
  -keyout $domain.key \
  -x509 \
  -days 365 \
  -out $domain.crt

echo "The $domain.crt is the public key (certificate signing request)"
echo "The $domain.key is the private key of RSA key pair"
echo
echo "sudo mkdir -p /etc/ssl/certs/iot /etc/ssl/certs/iot/private"
echo "sudo cp $domain.crt /etc/ssll/certs/iot"
echo "sudo cp $domain.key /etc/ssl/certs/iot/private"
