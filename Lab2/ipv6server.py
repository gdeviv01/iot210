#!/usr/bin/python
# =============================================================================
#        File : ipv4_tcp_client.py
# Description : TCP client using sockets
#      Author : Drew Gislsason
#        Date : 3/8/2017
# =============================================================================
import socket
import sys

# ipv4_tcp_client message [ip_addr [port]]
print "\nipv6server [ip_addr [port]]"

# optional IP address
if len(sys.argv) > 1:
  HOST_IP = sys.argv[1]
else:
  HOST_IP = "::"

# optional port
if len(sys.argv) > 2:
  PORT = sys.argv[2]
else:
  PORT = 5000
  
print "Listening on IPv6 UDP port ", PORT

sock = socket.socket(socket.AF_INET6, # IPv6 Internet
            socket.SOCK_DGRAM) # UDP
sock.bind((HOST_IP, PORT))

while True:
  data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
  print "received (" + str(len(data)) + ") bytes from " + str(addr) + ": ", str(data) + "\n"
