#!/usr/bin/python
# =============================================================================
#        File : ipv6client.py
# Description : UDP IPv6 client using sockets
#      Author : Drew Gislsason
#        Date : 3/8/2017
# =============================================================================
import socket
import sys

if len(sys.argv) < 2:
  print "ipv6client message [ip_addr [port]]"
  exit()

# message is first argument
MESSAGE = sys.argv[1]

# IPv6 address (e.g. fe80::68e4:5bb7:4b84:cd5)
if len(sys.argv) > 2:
  HOST_IP = sys.argv[2]
else:
  HOST_IP = "::1" 

# port
if len(sys.argv) > 3:
  PORT = int(sys.argv[3])
else:
  PORT = 5000

print "\nipv6client"
print "Target Host IP:", HOST_IP
print "Target Port:", PORT
print "Message:", MESSAGE


sock = socket.socket(socket.AF_INET6, # Internet
          socket.SOCK_DGRAM) # UDP
sock.sendto(MESSAGE, (HOST_IP, PORT))
