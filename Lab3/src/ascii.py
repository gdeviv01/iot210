#!/usr/bin/python
# =============================================================================
#        File : ascii.py
# Description : TCP client using sockets
#      Author : Drew Gislsason
#        Date : 4/7/2017
# =============================================================================
# pip install paho-mqtt
# American Standard Coding Information Interchange

def PrintAsciiTable(verbose=True):
  if verbose:
    for i in range(32,127):
      print str(hex(i)[2:]).rjust(3) + "='" + chr(i) + "'",
      if (i + 1) % 8 == 0:
        print ''
  else:
    for i in range(32,127):
      if i % 16 == 0:
        print str(hex(i)[2:]).rjust(3) + ':',
      print chr(i),
      if (i + 1) % 8 == 0:
        print ''    
  print ''

PrintAsciiTable()
