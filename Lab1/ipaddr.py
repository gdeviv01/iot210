#!/usr/bin/python
# =============================================================================
#        File : ipaddr.py
# Description : Displays IP addr on the Raspberry Pi SenseHat 8x8 LED display
#               e.g. '10.193.72.242'
#      Author : Drew Gislsason
#        Date : 3/1/2017
# =============================================================================

# To use on bootup of :
# sudo nano /etc/rc.local add line: python /home/pi/ipaddr.py &
#
import smtplib

from sense_hat import SenseHat
sense = SenseHat()
sense.set_rotation(180)
sense.show_letter('P')

# give time to get on the Wi-Fi network
import time
time.sleep(10)

# get our local IP address. Requires internet access (to talk to gmail.com).
import socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("gmail.com",80))
addr = s.getsockname()[0]
s.close()

#collect sensehat information
t = sense.get_temperature()
p = sense.get_pressure()
h = sense.get_humidity()

t = round(t,1)
p = round(p,1)
h = round(h,1)

# display that address on the sense had
if t > 18.3 and t < 26.7:
    bg = [0,100,0] #green
else:
    bg = [100,0,0] #red

#sense.show_message(str(t))
ip = str(addr)
sense.show_message(ip,back_colour=bg)
sense.show_message("*",back_colour=[0,0,0])

#send Pi's info to my email address
server = smtplib.SMTP('smtp.gmail.com', 587)
server.starttls()
server.login("devivero@thegicleeartstudio.com", "tempPASS")

msg = "IP_Adress = %s, Temperature = %s, Pressure = %s, Humidity = %s" %(ip,t,p,h)
server.sendmail("devivero@thegicleeartstudio.com", "devivero@gmail.com", msg)
server.quit()
